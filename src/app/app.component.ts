import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
interface Fruit {
  name: string;
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'MaterialForm';
  selectedValue: string | undefined;
  checked = false;
  contactForm !: FormGroup;
  isFormValid:boolean = false

  constructor(private _fb: FormBuilder) { }
  ngOnInit(): void {
     this.contactForm = this._fb.group({
       name:['', [Validators.required,Validators.pattern("^[A-Za-z]*$")]],
       dob:['', Validators.required],
       telphone:['', [Validators.required,Validators.minLength(10), Validators.maxLength(10)]],
       fruits:['',Validators.required],
       address:['', Validators.required],
       gender:['', Validators.required],
       check:['', Validators.required],
     })
  }

  get f(){
    return this.contactForm.controls;
  }

     fruits: Fruit[] = [
    {name: 'Apple' },
    {name: 'Banana' },
    {name: 'Pineapple' },
    {name: 'Grapes' },
    {name: 'Guavava' },
    {name: 'Orange' },
  
  ];
  telfun (event:any){
    console.log(event.target.value)
  console.log(event.key,22);
  console.log(event.keyCode,33);
  
  
    if (!/^[0-9]/.test(event.key) && event.keyCode !== 8) {
    
      event.preventDefault();
    }
  };

  onSubmit(){
     this.isFormValid = true;
     if(this.contactForm.valid){
        console.log(this.contactForm.value);
        alert("rffgtrbf")
     }
  }
}
